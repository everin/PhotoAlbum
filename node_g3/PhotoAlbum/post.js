// MySQL
var mysql = require('mysql');
var multipart = require('aws-lambda-multipart-parser');

// Crea connessione al nostro database MySQL
function getMySQLConnection() {
	return mysql.createConnection({
		host: process.env.RDS_HOSTNAME,
		port: process.env.RDS_PORT,
		user: process.env.RDS_USERNAME,
		password: process.env.RDS_PASSWORD,
		database: process.env.RDS_DB_NAME
	});
}

exports.handler = (event, context, callback) => {
	var response = {};
	// Gestione del body di tipo "multipart/form-data"
	var body =  multipart.parse(event, false);
	
	console.log('Prova LOG post');
	
	// Connessione al database MySQL
	var connection = getMySQLConnection();
	connection.connect();

    connection.query('insert into PhotoAlbum3 (id, username, description, photo) values (?, ?, ?, ?)', 
			[null, body.username, body.description, new Buffer.from(body.photo.content)], 
			function(error, result) {
	  	if (error) {
			// Alimento la risposta negativa
			response.statusCode = error.statusCode || 500;
			response.headers = { 'Content-Type': 'text/plain' };
			response.body = 'Internal server error';
			
			// Invio del messaggio negativo
			callback(error, response);
	  	} else {
			// Alimento la risposta positiva
			response.statusCode = 200;
			response.headers = { 'Content-Type': 'application/json' };
			response.body = JSON.stringify(result);
			
			// Invio del messaggio positivo
			callback(null, response);
		}
	});

	// Chiude la connessione al database MySQL
	connection.end();
};